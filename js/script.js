$(function(){
	
	$.fk={
		type:'POST',
		model:{},
		send:function(url,success,error){
			success=success||function(){};
			error=error||function(data){console.log(data);};
			$.ajax({ 
				url: url, 
				type: (this.type||'POST').toLocaleUpperCase(), 
				data: JSON.stringify(this.model||{}), 
				contentType: "application/json;charset=utf-8", 
				success: function(data){
                    if(typeof(data)=='object'){
                        success(data);
                    }else{
                        success($.parseJSON(data));
                    }
				}, 
				error: function(data){error(data);}
			});
		}
	};
	//===========================================
	$.general={
		chkFrm:function(array){
			var err=true;
			for(var i=0;i<array.length;i++){
				$(array[i]).removeClass('err');
				if($.trim($(array[i]).val())==''){
					err=false;
					$(array[i]).addClass('err');
				}
			}
			return err;
		},
        dialog:function(id,htmlLocation,callBack){
            var frm=$.find(id);
            callBack=callBack||function(){};
            if(frm.length<=0){
                 $.get('dialog.html',function(html_dialog){
                     $.get(htmlLocation,function(html){
                         html=html_dialog.replace('{id}',id.replace('#','')).replace('{body}',html).replace('{footer}','');
                         $('body').append(html);
                         frm=$.find(id);
                         $(frm).modal({
                            backdrop:'static',
                            show:true
                         });
                         callBack(frm);
                    });
                });
            }else{
                $(frm).modal({
                    backdrop:'static',
                    show:true
                });
                callBack(frm);
            }
            return frm;
        }
	};
	//===========================================
	$.supplier={
		register_frm:function(){
            //$.general.dialog('#frm_supplier','frm_supplier.html');
            var frm=$.find('#frm_supplier');
            if(frm.length<=0){
                 $.get('frm_supplier.html',function(html){
                     $('body').append(html);
                     frm=$.find('#frm_supplier');
                     $(frm).modal({
                         backdrop:'static',
                         show:true
                     });
                });
            }else{
                $(frm).modal({
                     backdrop:'static',
                     show:true
                 });
            }
		},
		register_submit:function(){
			var err=true;
			var user=$.trim($('#frm_supplier #user').val());
			var pass=$.trim($('#frm_supplier #password').val());
			var passConfirm=$.trim($('#frm_supplier #passwordConfirm').val());
			var agreement=$('#frm_supplier #agreement').prop('checked');
			
			err=$.general.chkFrm(['#frm_supplier #user','#frm_supplier #password','#frm_supplier #passwordConfirm']);
			err=(pass==passConfirm && err?err:false);
				if(!err){$('#frm_supplier #passwordConfirm').addClass('err');}
			err=(agreement && err?err:false);
			
			if(err){
				$.fk.model={
					user:user,
					pass:pass
				};
				$.fk.send('t.php',function(){
					$('#frm_supplier').on('hidden.bs.modal',function(){
                        if(err){
                            err=false;
                            $.general.dialog('#frm_supplier_info','frm_supplier_info.html',function(dialog){
                                $(dialog).find('.modal-footer').remove();
                                $(dialog).find('.modal-body').css('padding',0);
                                $('#frm_supplier [data-toggle="tooltip"]').popover({trigger:'hover',html:true,template:'<div class="popover" role="tooltip" style="border-radius:0;width:15em;"><div class="arrow"></div><div class="popover-content"></div></div>'});
                            });
                        }
                   });
					$('#frm_supplier').modal('hide');
				});
			}
		},
        register_submit_step2:function(){
            $('#frm_supplier_info').on('hidden.bs.modal',function(){
                $('#frm_supplier_info').off('hidden.bs.modal');
                $.general.dialog('#frm_supplier_info2','frm_supplier_info2.html',function(dialog){
                    $(dialog).find('.modal-footer').remove();
                    $(dialog).find('.modal-body').css('padding',0);
                });
            });
            $('#frm_supplier_info').modal('hide');
        },
        register_submit_step2_back:function(){
            $('#frm_supplier_info2').on('hidden.bs.modal',function(){
                $('#frm_supplier_info2').off('hidden.bs.modal');
                $.general.dialog('#frm_supplier_info','frm_supplier_info.html',function(dialog){
                    $(dialog).find('.modal-footer').remove();
                    $(dialog).find('.modal-body').css('padding',0);
                    $('[data-toggle="tooltip"]').popover({trigger:'hover',html:true,template:'<div class="popover" role="tooltip" style="border-radius:0;width:15em;"><div class="arrow"></div><div class="popover-content"></div></div>'});
                });
            });
            $('#frm_supplier_info2').modal('hide');
        },
        register_submit_step_final:function(){
            $('#frm_supplier_info2').off('hidden.bs.modal');
            $('#frm_supplier_info2').modal('hide');
        }
	};
	//===========================================
	$.restaurant={
		register_frm:function(){
			var frm=$.find('#frm_restaurant');
			if(frm.length<=0){
				$.get('frm_restaurant.html',function(html){
                    $('body').append(html);
                    frm=$.find('#frm_restaurant');
					$(frm).modal({
                         backdrop:'static',
                         show:true
                     });
				});
			}else{
				$(frm).modal({
                     backdrop:'static',
                     show:true
                 });
			}
		},
		register_submit:function(){
            var err=true;
			var user=$.trim($('#frm_restaurant #user').val());
			var pass=$.trim($('#frm_restaurant #password').val());
			var passConfirm=$.trim($('#frm_restaurant #passwordConfirm').val());
			var agreement=$('#frm_restaurant #agreement').prop('checked');
			
			err=$.general.chkFrm(['#frm_restaurant #user','#frm_restaurant #password','#frm_restaurant #passwordConfirm']);
			err=(pass==passConfirm && err?err:false);
				if(!err){$('#frm_restaurant #passwordConfirm').addClass('err');}
			err=(agreement && err?err:false);
			
			if(err){
				$.fk.model={
					user:user,
					pass:pass
				};
				$.fk.send('t.php',function(){
					$('#frm_restaurant').on('hidden.bs.modal',function(){
                        if(err){
                            err=false;
                            $.general.dialog('#frm_restaurant_info','frm_restaurant_info.html',function(dialog){
                                $(dialog).find('.modal-footer').remove();
                                $(dialog).find('.modal-body').css('padding',0);
                                $('#frm_restaurant [data-toggle="tooltip"]').popover({trigger:'hover',html:true,template:'<div class="popover" role="tooltip" style="border-radius:0;width:15em;"><div class="arrow"></div><div class="popover-content"></div></div>'});
                            });
                        }
                   });
					$('#frm_restaurant').modal('hide');
				});
			}
		},
        register_submit_step2:function(){
            $('#frm_restaurant_info').on('hidden.bs.modal',function(){
                $('#frm_restaurant_info').off('hidden.bs.modal');
                $.general.dialog('#frm_restaurant_info2','frm_restaurant_info2.html',function(dialog){
                    $(dialog).find('.modal-footer').remove();
                    $(dialog).find('.modal-body').css('padding',0);
                });
            });
            $('#frm_restaurant_info').modal('hide');
        },
        register_submit_step2_back:function(){
            $('#frm_restaurant_info2').on('hidden.bs.modal',function(){
                $('#frm_restaurant_info2').off('hidden.bs.modal');
                $.general.dialog('#frm_restaurant_info','frm_restaurant_info.html',function(dialog){
                    $(dialog).find('.modal-footer').remove();
                    $(dialog).find('.modal-body').css('padding',0);
                    $('#frm_restaurant_info [data-toggle="tooltip"]').popover({trigger:'hover',html:true,template:'<div class="popover" role="tooltip" style="border-radius:0;width:15em;"><div class="arrow"></div><div class="popover-content"></div></div>'});
                });
            });
            $('#frm_restaurant_info2').modal('hide');
        },
        register_submit_step_final:function(){
            $('#frm_restaurant_info2').off('hidden.bs.modal');
            $('#frm_restaurant_info2').modal('hide');
        }
	};
	
});